const doc = document;

const beforeSortButton = doc.getElementById("beforeSort");
const afterSortButton = doc.getElementById("afterSort");
const backButton = doc.getElementById("back");

if (beforeSortButton) {
	beforeSortButton.onclick = function() {
		window.location.href = "/beforeSort";
	};
}

if (afterSortButton) {
	afterSortButton.onclick = function() {
		window.location.href = "/afterSort";
	};
}

if (backButton) {
	backButton.onclick = function() {
		window.location.href = "/";
	};
}
