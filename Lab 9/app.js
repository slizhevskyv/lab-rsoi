const express = require("express");
const db = require("./db");

const app = express();

app.use(express.static(__dirname));
app.set("views", __dirname + "/views");
app.set("view engine", "pug");

app.get("/", function(req, res) {
	db.openConnection(function() {
		res.render("index", {});
	});
});

app.get("/beforeSort", function(req, res) {
	db.getAllInstruments().then(function(data) {
		res.render("sortView", { instruments: data[0].instruments, sort: false });
	});
});

app.get("/afterSort", function(req, res) {
	db.getAllInstruments().then(function(data) {
		let instruments = data[0].instruments.sort();
		instruments = instruments.map(item => item.toLowerCase());
		res.render("sortView", { instruments: instruments, sort: true });
	});
});

app.listen(3001);
