const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const instrumentsSchema = new Schema({
	instruments: [String]
});
const Instruments = mongoose.model("Instruments", instrumentsSchema);

function openConnection(callback) {
	mongoose
		.connect(
			"mongodb://localhost/laba9",
			{
				useNewUrlParser: true
			}
		)
		.then(callback)
		.catch(function(err) {
			console.log(err);
		});
}
function addInstruments(obj) {
	const instruments = new Instruments(obj);
	instruments
		.save()
		.then(function() {
			console.log("Данные сохранены!");
		})
		.catch(function(err) {
			console.log("При сохранении данных произошла ошибка!: " + err);
		});
}
function closeConnection() {
	mongoose.disconnect();
}

function getAllInstruments() {
	return new Promise(function(res, rej) {
		Instruments.find()
			.then(function(docs) {
				res(docs);
			})
			.catch(function(err) {
				console.log("Ошибка получения данных: " + err);
			});
	});
}

module.exports = {
	openConnection: openConnection,
	closeConnection: closeConnection,
	addInstruments: addInstruments,
	getAllInstruments: getAllInstruments
};
