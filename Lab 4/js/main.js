var doc = window.document;

var formWrapper = doc.getElementsByClassName("form-wrapper")[0],
	resultWrapper = doc.getElementsByClassName("result-wrapper")[0];

var submitButton = doc.getElementById("submitButton");

var username = doc.getElementById("username"),
	surname = doc.getElementById("surname"),
	age = doc.getElementById("age"),
	smartphones = doc.getElementById("smartphones"),
	wishes = doc.getElementById("wishes");

submitButton.addEventListener("click", handleSubmitAction);

function handleSubmitAction(event) {
	if (
		!doc.querySelector('input[name="sex"]:checked') ||
		!username.value ||
		!surname.value ||
		!age.value ||
		!wishes.value ||
		getSmartphones(smartphones).length === 0
	) {
		alert("Заполните все поля!");
		return false;
	}

	var sexValue = "Пол:" + doc.querySelector('input[name="sex"]:checked').value,
		usernameValue = "Имя: " + username.value,
		surnameValue = "Фамилия: " + surname.value,
		ageValue = "Возраст: " + age.value,
		wishesValue = "Пожелания: " + wishes.value,
		smartphonesValue = getSmartphones(smartphones).join(", ");

	var userNameDiv = doc.createElement("div");
	userNameDiv.innerHTML = usernameValue;
	resultWrapper.appendChild(userNameDiv);

	var surnameNameDiv = doc.createElement("div");
	surnameNameDiv.innerHTML = surnameValue;
	resultWrapper.appendChild(surnameNameDiv);

	var ageDiv = doc.createElement("div");
	ageDiv.innerHTML = ageValue;
	resultWrapper.appendChild(ageDiv);

	var sexDiv = doc.createElement("div");
	sexDiv.innerHTML = sexValue;
	resultWrapper.appendChild(sexDiv);

	var wishesDiv = doc.createElement("div");
	wishesDiv.innerHTML = wishesValue;
	resultWrapper.appendChild(wishesDiv);

	var smartphonesDiv = doc.createElement("div");
	smartphonesDiv.innerHTML =
		"Смартфоны, которые понравились: " + smartphonesValue;
	resultWrapper.appendChild(smartphonesDiv);

	formWrapper.style.display = "none";
	resultWrapper.style.display = "flex";
}

function getSmartphones(items) {
	var result = [];
	for (var i = 0; i < items.length; i++) {
		var smartphone = items[i];
		if (smartphone.selected) {
			result.push(smartphone.text);
		}
	}
	return result;
}
