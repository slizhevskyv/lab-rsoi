export default (salary, ability) => {
    switch (ability) {
        case "Высокие":
            return (salary * 30) / 100;
        case "Средние":
            return (salary * 20) / 100;
        case "Низкие":
            return (salary * 10) / 100;
    }
};
