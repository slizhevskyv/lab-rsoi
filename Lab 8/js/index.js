import "../css/main.css";
import template from "../templates/template.pug";
import templateSummary from "../templates/summary.pug";
import abilityModule from "./abilities_m.js";
import categoryModule from "./category_m.js";
import {
    getFilteredDataByPropertyName,
    getEmployeesByDepartment,
    getEmployeeIDByName
} from "./helper.js";

const doc = document;
const container = doc.getElementsByClassName("container")[0];

let departmentSelect,
    employeeSelect,
    abilitiesRadio,
    categorySelect,
    rateInput,
    summaryDiv,
    averageSalaryButton,
    saveButton;

const departments = getFilteredDataByPropertyName("department");
const employees = getFilteredDataByPropertyName("name");

doc.addEventListener("DOMContentLoaded", () =>
    initHtml()
        .then(initListeners)
        .catch(err => {
            console.log(err);
        })
);

const initHtml = () => {
    return new Promise((resolve, reject) => {
        container.innerHTML = template({
            departments: departments,
            departmentLabel: "Департамент",
            departmentID: "departments",
            employees: employees,
            employeesLabel: "Работник",
            employeesID: "employees"
        });
        resolve();
    });
};

const initListeners = () => {
    departmentSelect = doc.getElementById("departments");
    employeeSelect = doc.getElementById("employees");
    categorySelect = doc.getElementById("category");
    rateInput = doc.getElementById("rate");
    averageSalaryButton = doc.getElementById("averageSalary");
    summaryDiv = doc.getElementById("summary");
    saveButton = doc.getElementById("save");

    departmentSelect.onchange = event => {
        employeeSelect.dispatchEvent(
            new CustomEvent("selectDepartment", {
                detail: {
                    selectedDepartment:
                        event.target.options[event.target.options.selectedIndex]
                            .value
                }
            })
        );
    };
    employeeSelect.addEventListener("selectDepartment", event => {
        const selectedDepartment = event.detail.selectedDepartment;
        const employeesByDepartment = getEmployeesByDepartment(
            selectedDepartment
        );
        removeAllChilds(employeeSelect);
        if (selectedDepartment === "...") {
            appendOptions(employeeSelect, employees);
        }
        appendOptions(employeeSelect, employeesByDepartment);
    });
    employeeSelect.onchange = () => {
        const employeeName = getSelectedElement(employeeSelect);
        if (employeeName === "...") {
            return;
        }
        const employeeInfo = localStorage.getItem(
            getEmployeeIDByName(employeeName)
        );
        if (employeeInfo) {
            summaryDiv.innerHTML = templateSummary(JSON.parse(employeeInfo));
        } else {
            summaryDiv.innerHTML = "Нет данных о работнике: " + employeeName;
        }
    };
    saveButton.onclick = () => {
        abilitiesRadio = doc.querySelector("input[name='abilities']:checked");
        if (!validateData(abilitiesRadio)) {
            alert("Invalid data!");
            return;
        }
        const summaryInfo = getSummaryObject();
        localStorage.setItem(
            getEmployeeIDByName(summaryInfo.employeeName),
            JSON.stringify(summaryInfo)
        );
        summaryDiv.innerHTML = "";
        clearInputs();
    };
    averageSalaryButton.onclick = () => {
        const selectedDepartment = getSelectedElement(departmentSelect);
        if (selectedDepartment === "...") {
            return;
        }
        const dataFromStorage = allStorage();
        const averageSalary = dataFromStorage
            .filter(item => item.department === selectedDepartment)
            .reduce((acc, item) => {
                acc = acc + item.salary;
                return acc;
            }, 0);
        summaryDiv.innerHTML =
            'Средняя зарплата по отделу "' +
            selectedDepartment +
            '" составляет: ' +
            averageSalary;
    };
};

const allStorage = () => {
    let values = [],
        keys = Object.keys(localStorage),
        i = keys.length;
    while (i--) {
        const item = localStorage.getItem(keys[i]);
        if (item !== "INFO") {
            values.push(JSON.parse(item));
        }
    }
    return values;
};

const removeAllChilds = elem => {
    while (elem.firstChild) {
        elem.removeChild(elem.firstChild);
    }
};

const appendOptions = (elem, data) => {
    const option = doc.createElement("option");
    option.setAttribute("value", "...");
    option.text = "...";
    elem.appendChild(option);
    data.forEach(item => {
        const option = doc.createElement("option");
        option.setAttribute("value", item);
        option.text = item;
        elem.appendChild(option);
    });
};

const getSelectedElement = elem => {
    return elem.options[elem.options.selectedIndex].value;
};

const setDefaultSelectValue = elem => {
    return (elem.options.selectedIndex = 0);
};

const getSummaryObject = () => {
    const rate = +rateInput.value,
        category = getSelectedElement(categorySelect),
        abilities = abilitiesRadio.value,
        department = getSelectedElement(departmentSelect),
        employeeName = getSelectedElement(employeeSelect);
    return {
        employeeName,
        department,
        abilities,
        category,
        rate,
        salary:
            rate +
            abilityModule(rate, abilities) +
            categoryModule(rate, category)
    };
};

const validateData = radio => {
    return (
        getSelectedElement(employeeSelect) !== "..." &&
        getSelectedElement(departmentSelect) !== "..." &&
        !!radio &&
        !!rateInput.value &&
        !isNaN(rateInput.value)
    );
};

const clearInputs = () => {
    rateInput.value = "";
    abilitiesRadio.checked = false;
    setDefaultSelectValue(categorySelect);
    setDefaultSelectValue(departmentSelect);
    setDefaultSelectValue(employeeSelect);
};
