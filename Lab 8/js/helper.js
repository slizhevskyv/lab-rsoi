import Data from "../content/content.json";

export const getFilteredDataByPropertyName = propertyName => {
    return Data.reduce((acc, dataItem) => {
        const propertyValue = dataItem[propertyName];
        if (!acc.includes(propertyValue)) acc.push(propertyValue);
        return acc;
    }, []);
};

export const getEmployeesByDepartment = department => {
    return Data.reduce((acc, dataItem) => {
        dataItem.department === department ? acc.push(dataItem.name) : "";
        return acc;
    }, []);
};

export const getEmployeeIDByName = name => {
    return Data.find(item => item.name === name).id;
};
