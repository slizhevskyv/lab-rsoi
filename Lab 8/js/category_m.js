export default (salary, category) => {
    switch (category) {
        case "1":
            return (salary * 5) / 100;
        case "2":
            return (salary * 10) / 100;
        case "3":
            return (salary * 15) / 100;
        case "4":
            return (salary * 20) / 100;
        case "5":
            return (salary * 25) / 100;
    }
};
