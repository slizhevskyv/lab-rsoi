let db = openDatabase("schoolDB", "1.0", "School Database", 200000);

const initDB = () => {
    db.transaction(transaction => {
        transaction.executeSql(
            "CREATE TABLE IF NOT EXISTS school (" +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "classNumber TEXT NOT NULL, classAmount TEXT NOT NULL," +
                "classroomTeacherFIO TEXT NOT NULL, classroomTeacherTelephone TEXT NOT NULL);"
        );
    });
};

const addRecord = function(data, successCallback) {
    db.transaction(function(transaction) {
        transaction.executeSql(
            "INSERT INTO school (classNumber, classAmount, classroomTeacherFIO, classroomTeacherTelephone) VALUES (?, ?, ?, ?);",
            [
                data.classNumber,
                data.classAmount,
                data.classroomTeacherFIO,
                data.classroomTeacherTelephone
            ],
            function(transaction, results) {
                successCallback(results);
            }
        );
    });
};

const loadRecords = function(successCallback) {
    db.transaction(function(transaction) {
        transaction.executeSql("SELECT * FROM school;", [], function(
            transaction,
            results
        ) {
            successCallback(results);
        });
    });
};

const deleteRecord = function(id, successCallback) {
    db.transaction(function(transaction) {
        transaction.executeSql(
            "DELETE FROM school WHERE id = ?",
            [id],
            function(transaction, results) {
                successCallback(results);
            }
        );
    });
};

export { initDB, addRecord, loadRecords, deleteRecord };
