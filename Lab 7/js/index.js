import "../css/main.css";
import { initDB, addRecord, loadRecords, deleteRecord } from "./dbService.js";
import formTemplate from "../templates/form.pug";
import deleteTemplate from "../templates/delete.pug";
import tableTemplate from "../templates/table.pug";

const doc = document;
const addButton = doc.getElementById("add");
const showButton = doc.getElementById("show");
const deleteButton = doc.getElementById("delete");
const content = doc.getElementById("content");

doc.addEventListener("DOMContentLoaded", () => {
    initDB();
});

addButton.onclick = () => {
    content.innerHTML = formTemplate();
    const addRecordButton = doc.getElementById("write-record");
    const clearFormButton = doc.getElementById("clear");
    addRecordButton.onclick = () => {
        const classNumber = doc.getElementById("class-number").value;
        const classAmount = doc.getElementById("class-amount").value;
        const classroomTeacherFIO = doc.getElementById("class-classroomTeacher")
            .value;
        const classroomTeacherTelephone = doc.getElementById(
            "class-classroomTeacherTelephone"
        ).value;

        addRecord(
            {
                classNumber,
                classAmount,
                classroomTeacherFIO,
                classroomTeacherTelephone
            },
            () => {
                alert("Запись успешно добавлена!");
                clearForm();
            }
        );
    };

    clearFormButton.onclick = () => {
        clearForm();
    };
};

showButton.onclick = () => {
    loadRecords(result => {
        const rows = [].slice.call(result.rows);
        content.innerHTML = tableTemplate({ rows });
    });
};

deleteButton.onclick = () => {
    content.innerHTML = deleteTemplate();
    const selectInput = doc.getElementById("delete-select");
    const deleteRecordButton = doc.getElementById("delete-record");
    loadRecords(result => {
        const options = [].slice.call(result.rows).reduce((acc, item) => {
            acc.push(item.id);
            return acc;
        }, []);
        options.forEach(item => {
            const option = createOptions(item);
            selectInput.append(option);
        });
    });
    deleteRecordButton.onclick = () => {
        const id = selectInput.options[selectInput.selectedIndex].text;
        deleteRecord(id, () => {
            alert("Record removed!");
            showButton.dispatchEvent(new Event("onclick"));
        });
    };
};

const clearForm = () => {
    const inputs = [...doc.getElementsByClassName("class-input")];
    inputs.forEach(item => (item.value = ""));
};

const createOptions = data => {
    const option = doc.createElement("option");
    option.setAttribute("value", data);
    option.text = data;
    return option;
};

if (window.location.href === "http://localhost:8080/delete") {
}
