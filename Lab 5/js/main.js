var doc = document;

var clearButton = doc.getElementById("clearButton");
var addButton = doc.getElementById("addTextFieldButton");
var showInfoButton = doc.getElementById("showInfoButton");

var popUpContent = doc.getElementsByClassName("pop-up-content")[0];
var popUpCloseBtn = doc.getElementById("pop-up-close-btn");
var popUpWrapper = doc.getElementsByClassName("pop-up-wrapper")[0];

var wrapper = doc.getElementsByClassName("text-fields-wrapper")[0];

var formGroupTemplate = doc.getElementsByClassName("form-group")[0];
var textFields = doc.getElementsByClassName("text-field");

var fontInput = doc.getElementById("font");

clearButton.onclick = function() {
	var i = 0;
	for (i; i < textFields.length; i++) {
		textFields[i].value = "";
	}
};

addButton.onclick = function() {
	var length = textFields.length;
	var id = "textField" + (length + 1);
	var newFormGroup = formGroupTemplate.cloneNode(true);
	newFormGroup.getElementsByClassName("text-field")[0].id = id;
	newFormGroup.getElementsByClassName("text-field")[0].value = "";
	newFormGroup.querySelector("label").htmlFor = id;
	newFormGroup.querySelector("label").innerHTML =
		"Текстовой поле " + (length + 1) + ":";
	wrapper.appendChild(newFormGroup);
};

showInfoButton.onclick = function() {
	popUpWrapper.classList.remove("pop-up-hide");
	var span = doc.createElement("span"),
		fieldsText = "",
		font = fontInput[fontInput.selectedIndex].text;
	[].forEach.call(textFields, function(field, index) {
		if (field.value === "") {
			fieldsText += "Поле " + (index + 1) + ": Ничего не было написано :C\n";
			return;
		}
		fieldsText += "Поле " + (index + 1) + ": " + field.value + "\n";
	});
	span.innerHTML = fieldsText;
	span.style.fontSize = font + "px";
	popUpContent.appendChild(span);
};

popUpCloseBtn.onclick = function() {
	popUpWrapper.classList.add("pop-up-hide");
	popUpContent.getElementsByTagName("span")[0].remove();
};
