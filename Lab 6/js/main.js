$(function() {
	var $textField1 = $("#textField1");
	var $textField2 = $("#textField2");
	var $textField3 = $("#textField3");
	var $textField4 = $("#textField4");
	var $textField5 = $("#textField5");

	var $liItems = $(".ul-wrapper ul li");

	var $changeButton = $("#changeButton");
	var $olAnimateButton = $("#animateUl");

	$changeButton.on("click", changeButtonHandler);
	$olAnimateButton.on("click", olAnimateButtonHandler);

	function changeButtonHandler() {
		var cssObject = {
			"font-size": "20px",
			color: "red"
		};
		var text5 = $textField5.val();
		$textField1.val(text5);
		$textField3.val(text5);
		$textField2.css(cssObject);
		$textField4.css(cssObject);
	}

	function olAnimateButtonHandler() {
		var i = 0;
		console.log($liItems.length);
		var timerID = setTimeout(function init() {
			if (i % 2 === 0) {
				$($liItems.get(i)).hide(700, function() {
					i += 1;
					init();
				});
				return;
			}
			i += 1;
			i < $liItems.length ? init() : clearTimeout(timerID);
		}, 1);
	}
});
